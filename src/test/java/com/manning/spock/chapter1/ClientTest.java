package com.manning.spock.chapter1;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import com.manning.spock.chapter1.billing.Client;
import com.manning.spock.chapter1.billing.CreditCardBilling;

public class ClientTest {

	private CreditCardBilling billing = new CreditCardBilling();
	private Client client = new Client();

	@Test
	public void shouldGotBonusWhenSpendMoreThen100Dollars() {

		// when
		billing.charge(client,150);

		// then
		assertTrue("expect bonus",client.hasBonus());
	}

	@Test
	public void shouldLoseBonusWhenItRejectsCharge()	{

		// when
		billing.charge(client,150);
		client.rejectsCharge();

		// then
		assertFalse("bonus not expected",client.hasBonus());
	}

}
