package groovy.com.manning.spock.chapter1.banking

import com.manning.spock.banking.Account
import spock.lang.Specification
import spock.lang.*

/**
 * Created by bogumil on 22.06.16.
 */
class BankingSpec extends Specification {


    def "Transferring money to a savings account"()
    {
        given: "my Current account has a balance of 1000.00"
            def currentAccount = new Account(1000.00)
        and: "my Savings account has a balance of 2000.00"
            def savingAccount = new Account(2000.00)
        when: "I transfer 500.00 from my Current account to my Savings account"
            currentAccount.sub(500.00)
            savingAccount.add(500.00)
        then: "Then I should have 500.00 in my Current account"
            currentAccount.getBalance() == 500.00
        and: "I should have 2500.00 in my Savings account"
            savingAccount.getBalance() == 2500.00
    }
}