package com.manning.spock.banking;

import java.math.BigDecimal;

/**
 * Created by bogumil on 22.06.16.
 */
public class Account {
    private double balance;

    public Account(double balance)
    {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void sub(double val) {
        this.balance -= val;
    }

    public void add(double val) {
        this.balance += val;
    }
}
